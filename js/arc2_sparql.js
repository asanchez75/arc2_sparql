(function($){
  Drupal.behaviors.arc2Sparql = {};
  Drupal.behaviors.arc2Sparql.attach = function(context, settings) {
      if (typeof Drupal.settings.arc2_sparql != 'undefined') {
        $('.block-arc2-sparql .content, .pane-arc2-sparql .pane-content').html('').addClass('loading');
        var nid = Drupal.settings.arc2_sparql.nid;
        var tag = Drupal.settings.arc2_sparql.tag;
        var url = Drupal.settings.basePath+'arc2_sparql/ajax/getResources/?nid='+nid+'&tag='+tag;
        $.get(url,
          function(data) {
           $('.block-arc2-sparql .content, .pane-arc2-sparql .pane-content').html(data);
           $('.block-arc2-sparql .content, .pane-arc2-sparql .pane-content').removeClass('loading')
          }
        );
        return false;
      }
  }
})(jQuery);

